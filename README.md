# 6轴机械臂带力传感器

#### 介绍
使用57步进电机制作的一台6轴机械臂，负载稳定1kg，带有力传感器，可以拖动示教，轨迹复现。

#### 软件架构
软件架构说明


#### 安装教程

1.  3D模型图打开软件FreeCAD
2.  程序打开软件stm32cubeide
3.  视频观看地址：https://www.bilibili.com/video/BV15q4y1s7aP/

#### 使用说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/150057_4a71cabc_2117144.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/150611_a8041edb_2117144.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/150725_495b9821_2117144.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/151045_32cb08f1_2117144.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/151206_85c38a5a_2117144.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0627/151302_a10c6a9b_2117144.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
